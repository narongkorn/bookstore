package com.mycompany.myproject.bookstore.exception

import org.springframework.context.MessageSourceResolvable
import org.springframework.http.HttpStatus

interface ApiExceptionMessageSource : MessageSourceResolvable {
    val errorCode: String
    val httpStatus: HttpStatus
}