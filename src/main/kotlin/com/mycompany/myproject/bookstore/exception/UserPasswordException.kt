package com.mycompany.myproject.bookstore.exception

import org.springframework.http.HttpStatus

class UserPasswordException : RuntimeException(), ApiExceptionMessageSource {
    override val errorCode: String = "U002"
    override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST
    override fun getCodes(): Array<String> = arrayOf("password_invalid")
}