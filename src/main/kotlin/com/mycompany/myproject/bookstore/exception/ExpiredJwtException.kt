package com.mycompany.myproject.bookstore.exception

import org.springframework.http.HttpStatus

class ExpiredJwtException : RuntimeException(), ApiExceptionMessageSource {
    override val httpStatus: HttpStatus = HttpStatus.UNAUTHORIZED
    override val errorCode = "E002"
    override fun getCodes() = arrayOf("jwt_expired")
}
