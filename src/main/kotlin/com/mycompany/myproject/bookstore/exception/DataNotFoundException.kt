package com.mycompany.myproject.bookstore.exception

import org.springframework.http.HttpStatus

class DataNotFoundException() : RuntimeException(), ApiExceptionMessageSource  {
    override val errorCode: String = "C001"
    override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST
    override fun getCodes(): Array<String> = arrayOf("data_not_found")
}
