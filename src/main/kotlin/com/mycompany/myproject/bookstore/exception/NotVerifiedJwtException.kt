package com.mycompany.myproject.bookstore.exception

import org.springframework.http.HttpStatus

class NotVerifiedJwtException(cause: Throwable? = null) : RuntimeException(cause), ApiExceptionMessageSource {
    override val httpStatus: HttpStatus = HttpStatus.UNAUTHORIZED
    override val errorCode = "E003"
    override fun getCodes() = arrayOf("jwt_not_verified")
}
