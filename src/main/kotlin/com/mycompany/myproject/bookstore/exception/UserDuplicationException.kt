package com.mycompany.myproject.bookstore.exception

import org.springframework.http.HttpStatus

class UserDuplicationException : RuntimeException(), ApiExceptionMessageSource {
    override val errorCode: String = "U001"
    override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST
    override fun getCodes(): Array<String> = arrayOf("user_duplicate")
}