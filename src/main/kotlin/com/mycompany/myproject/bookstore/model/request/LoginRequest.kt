package com.mycompany.myproject.bookstore.model.request

import javax.validation.constraints.NotEmpty

class LoginRequest(
    @field:NotEmpty
    val username: String,
    @field:NotEmpty
    val password: String
)