package com.mycompany.myproject.bookstore.model.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class BaseResponse<T>(
    var code: String,
    var message: String,
    var data: T?,
    var cause: String?
) {

    class Builder<T>() {

        fun success(data: T? = null): BaseResponse<T> {
            return BaseResponse("000", "SUCCESS", data, null)
        }

        fun error(code: String, message: String, data: T? = null, cause: String? = null): BaseResponse<T> {
            return BaseResponse(code, message, data, cause)
        }
    }
}
