package com.mycompany.myproject.bookstore.model.request


class OrderCreateRequest(
    val orders: List<Long> = listOf()
) {
    var userId: Long? = null
}