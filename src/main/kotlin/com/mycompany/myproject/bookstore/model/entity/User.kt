package com.mycompany.myproject.bookstore.model.entity

import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class User(
    @Id
    @GeneratedValue
    var id: Long? = null,

    @Column(unique = true)
    var username: String? = null,
    var password: String? = null,
    var name: String? = null,
    var surname: String? = null,
    var dateOfBirth: Date? = null
)