package com.mycompany.myproject.bookstore.model.response

class LoginResponse(
    val token: String
)