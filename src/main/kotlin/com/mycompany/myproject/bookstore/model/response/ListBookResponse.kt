package com.mycompany.myproject.bookstore.model.response

class ListBookResponse(
    val books : List<BookDto> = listOf()
)