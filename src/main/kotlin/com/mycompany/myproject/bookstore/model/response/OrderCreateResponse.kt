package com.mycompany.myproject.bookstore.model.response

import java.math.BigDecimal

class OrderCreateResponse(
    val price: BigDecimal
)