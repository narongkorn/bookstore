package com.mycompany.myproject.bookstore.model.response

import com.fasterxml.jackson.annotation.JsonFormat
import com.mycompany.myproject.bookstore.config.DATE_FORMAT
import java.util.Date

class UserDto(
    val name: String,
    val surname: String,
    @field:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    val dateOfBirth: Date?,
    val books: List<Long> = arrayListOf()
)