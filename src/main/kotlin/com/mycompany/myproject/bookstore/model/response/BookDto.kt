package com.mycompany.myproject.bookstore.model.response

import java.math.BigDecimal

class BookDto(
    val id: Long,
    val name: String,
    val author: String,
    val price: BigDecimal,
    val isRecommended: Boolean
)