package com.mycompany.myproject.bookstore.model.request

import com.fasterxml.jackson.annotation.JsonFormat
import com.mycompany.myproject.bookstore.config.DATE_FORMAT
import java.util.Date
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class UserCreateRequest(
    @field:Size(min = 3, max = 50)
    @field:Pattern(regexp = "^[a-zA-Z0-9]+\\.[a-zA-Z0-9]+")
    val username: String,
    @field:Size(min = 1, max = 50)
    val password: String,
    @field:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    val dateOfBirth: Date
)