package com.mycompany.myproject.bookstore.config

import com.mycompany.myproject.bookstore.util.getPKCS8EncodedKeySpec
import com.mycompany.myproject.bookstore.util.getX509EncodedKeySpec
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.convert.DurationUnit
import org.springframework.context.annotation.Configuration
import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.time.Duration
import java.time.temporal.ChronoUnit

@Configuration
@ConfigurationProperties(prefix = "token.security")
class JwsTokenConfig(
    @DurationUnit(ChronoUnit.MINUTES)
    var tokenExpirationTime: Duration = Duration.ofMinutes(5),
    var privateKey: String = "",
    var publicKey: String = ""
) {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    fun getRsaPublicKey(): RSAPublicKey {
        val spec = getX509EncodedKeySpec(publicKey)
        val kf = KeyFactory.getInstance("RSA")
        return kf.generatePublic(spec) as RSAPublicKey
    }

    fun getRsaPrivateKey(): RSAPrivateKey {
        val spec = getPKCS8EncodedKeySpec(privateKey)
        val kf = KeyFactory.getInstance("RSA")
        return kf.generatePrivate(spec) as RSAPrivateKey
    }
}