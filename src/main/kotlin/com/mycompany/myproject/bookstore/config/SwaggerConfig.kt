package com.mycompany.myproject.bookstore.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType.SWAGGER_2
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig {

    companion object {
        const val GROUP_NAME: String = "BOOKSTORE"
    }

    @Bean
    fun api(): Docket {
        return Docket(SWAGGER_2)
            .useDefaultResponseMessages(false)
            .groupName(GROUP_NAME)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.mycompany.myproject"))
            .build()
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder().title("Book Store API")
            .description("Book Store API")
            .version("1.0")
            .build()
    }
}
