package com.mycompany.myproject.bookstore.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.mycompany.myproject.bookstore.client.ExternalBookClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.TimeZone

@Configuration
@EnableFeignClients(
    clients = [
        ExternalBookClient::class
    ]
)
@EnableCaching
class AppConfig {

    @Autowired
    fun configureJackson(objectMapper: ObjectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault())
    }

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}