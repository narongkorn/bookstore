package com.mycompany.myproject.bookstore

import com.mycompany.myproject.bookstore.exception.ApiExceptionMessageSource
import com.mycompany.myproject.bookstore.model.response.BaseResponse
import org.springframework.context.MessageSource
import org.springframework.context.MessageSourceResolvable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.Locale

@ControllerAdvice
class RestResponseEntityExceptionHandler(
    val messageSource: MessageSource
) : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    fun defaultException(ex: Exception, req: WebRequest, locale: Locale): ResponseEntity<BaseResponse<String>> {
        val errorCode = when (ex) {
            is ApiExceptionMessageSource -> ex.errorCode
            else -> "xxxx"
        }
        val message = when (ex) {
            is MessageSourceResolvable -> messageSource.getMessage(ex, locale)
            else -> messageSource.getMessage("default_error", null, locale)
        }
        val httpStatus = when (ex) {
            is ApiExceptionMessageSource -> ex.httpStatus
            else -> HttpStatus.INTERNAL_SERVER_ERROR
        }
        logger.error("defaultException: ", ex)
        val response =
            BaseResponse.Builder<String>().error(
                errorCode,
                message,
                null,
                ex.message
            )
        return ResponseEntity(response, httpStatus)
    }
}
