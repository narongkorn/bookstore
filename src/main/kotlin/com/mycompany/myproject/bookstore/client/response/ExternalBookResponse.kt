package com.mycompany.myproject.bookstore.client.response

import java.math.BigDecimal

class ExternalBookResponse(
    val id: Long? = null,
    val bookName: String? = null,
    val authorName: String? = null,
    val price: BigDecimal? = null
)