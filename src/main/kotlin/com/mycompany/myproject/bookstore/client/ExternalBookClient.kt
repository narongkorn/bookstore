package com.mycompany.myproject.bookstore.client

import com.mycompany.myproject.bookstore.client.response.ExternalBookResponse
import org.springframework.cache.annotation.Cacheable
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping

@FeignClient(
    value = "externalBookClient",
    url = "\${book.api.url}"
)
interface ExternalBookClient {

    @Cacheable("allBook")
    @GetMapping
    fun listAllBook(): List<ExternalBookResponse>

    @Cacheable("recommendedBook")
    @GetMapping("/recommendation")
    fun listRecommendedBook(): List<ExternalBookResponse>
}