package com.mycompany.myproject.bookstore.controller

import com.mycompany.myproject.bookstore.model.response.ListBookResponse
import com.mycompany.myproject.bookstore.service.BookService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/books")
class BookController(private val bookService: BookService) {

    @GetMapping
    fun listBook(): ListBookResponse {
        return bookService.listBook()
    }
}