package com.mycompany.myproject.bookstore.controller

import com.mycompany.myproject.bookstore.config.TOKEN
import com.mycompany.myproject.bookstore.model.request.OrderCreateRequest
import com.mycompany.myproject.bookstore.model.request.UserCreateRequest
import com.mycompany.myproject.bookstore.model.response.OrderCreateResponse
import com.mycompany.myproject.bookstore.model.response.UserDto
import com.mycompany.myproject.bookstore.security.JwsTokenService
import com.mycompany.myproject.bookstore.service.OrderService
import com.mycompany.myproject.bookstore.service.UserService
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UserController(
    private val userService: UserService,
    private val orderService: OrderService,
    private val tokenService: JwsTokenService
) {

    @PostMapping
    fun createUser(@Valid @RequestBody req: UserCreateRequest) {
        userService.createUser(req)
    }

    @DeleteMapping
    fun deleteUserLog(@RequestHeader(TOKEN) token: String) {
        val userId = tokenService.verifyToken(token).uid
        orderService.deleteAllOrder(userId)
    }

    @GetMapping
    fun getUserLog(@RequestHeader(TOKEN) token: String): UserDto {
        val userId = tokenService.verifyToken(token).uid
        return userService.getUser(userId)
    }

    @PostMapping("/orders")
    fun createOrder(@RequestHeader(TOKEN) token: String, @Valid @RequestBody req: OrderCreateRequest) : OrderCreateResponse {
        val userId = tokenService.verifyToken(token).uid
        req.userId = userId
        return orderService.createOrder(req)
    }
}