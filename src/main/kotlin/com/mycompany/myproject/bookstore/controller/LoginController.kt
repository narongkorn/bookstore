package com.mycompany.myproject.bookstore.controller

import com.mycompany.myproject.bookstore.model.request.LoginRequest
import com.mycompany.myproject.bookstore.model.response.LoginResponse
import com.mycompany.myproject.bookstore.service.LoginService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/login")
class LoginController(private val loginService: LoginService) {

    @PostMapping
    fun doLogin(@Valid @RequestBody req: LoginRequest): LoginResponse {
        return loginService.doLogin(req)
    }
}