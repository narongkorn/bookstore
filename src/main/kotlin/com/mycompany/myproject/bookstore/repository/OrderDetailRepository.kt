package com.mycompany.myproject.bookstore.repository

import com.mycompany.myproject.bookstore.model.entity.OrderDetail
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderDetailRepository : CrudRepository<OrderDetail, Long> {

    @Modifying
    @Query(value = "delete order_detail o where o.user_id = :userId", nativeQuery = true)
    fun deleteOrderDetail(userId: Long)
}