package com.mycompany.myproject.bookstore.repository

import com.mycompany.myproject.bookstore.model.entity.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<User, Long> {
    fun findByUsername(username: String) : User?
}