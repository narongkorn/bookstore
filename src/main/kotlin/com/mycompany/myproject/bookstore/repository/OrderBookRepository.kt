package com.mycompany.myproject.bookstore.repository

import com.mycompany.myproject.bookstore.model.entity.OrderBook
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderBookRepository : CrudRepository<OrderBook, Long> {

    @Query("select o from OrderBook o where o.order.user.id = :userId")
    fun findOrderBookByUserId(userId: Long): List<OrderBook>

    @Modifying
    @Query(
        value = "delete order_book o where o.id in " +
            "(select ob.id from order_detail od inner join order_book ob on od.id = ob.order_id where od.user_id = :userId)",
        nativeQuery = true
    )
    fun deleteOrderBook(userId: Long)
}