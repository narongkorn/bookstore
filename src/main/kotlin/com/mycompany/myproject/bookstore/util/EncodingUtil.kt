package com.mycompany.myproject.bookstore.util

import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.regex.Pattern
import javax.xml.bind.DatatypeConverter

fun getX509EncodedKeySpec(publicKey: String): X509EncodedKeySpec {
    return X509EncodedKeySpec(decodePem(publicKey))
}

fun getPKCS8EncodedKeySpec(privateKey: String): PKCS8EncodedKeySpec {
    return PKCS8EncodedKeySpec(decodePem(privateKey))
}

fun decodePem(key: String): ByteArray? {
    val parse = Pattern.compile("(?m)(?s)^---*BEGIN.*---*$(.*)^---*END.*---*$.*")
    val encoded = parse.matcher(key).replaceFirst("$1")
    return DatatypeConverter.parseBase64Binary(encoded)
}
