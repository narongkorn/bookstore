package com.mycompany.myproject.bookstore.security.model

class UserContext(
    val uid: Long,
    val username: String
)