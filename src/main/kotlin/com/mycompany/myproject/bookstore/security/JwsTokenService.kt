package com.mycompany.myproject.bookstore.security

import com.mycompany.myproject.bookstore.exception.ExpiredJwtException
import com.mycompany.myproject.bookstore.config.JwsTokenConfig
import com.mycompany.myproject.bookstore.exception.NotVerifiedJwtException
import com.mycompany.myproject.bookstore.security.model.UserContext
import com.nimbusds.jose.JOSEException
import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.crypto.RSASSAVerifier
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import org.springframework.stereotype.Service
import java.util.Date

@Service
class JwsTokenService(private val config: JwsTokenConfig) {

    companion object {
        const val UID = "uid"
        const val USERNAME = "username"
    }

    fun createToken(userContext: UserContext): String {
        return creatToken(jwtBuilder(userContext).build())
    }

    private fun creatToken(claims: JWTClaimsSet): String {
        val signedJWT = SignedJWT(JWSHeader.Builder(JWSAlgorithm.RS256).build(), claims)
        val signer = RSASSASigner(config.getRsaPrivateKey())
        signedJWT.sign(signer)
        return signedJWT.serialize()
    }

    private fun jwtBuilder(userContext: UserContext): JWTClaimsSet.Builder {
        val now = Date()
        val expireTime = now.time + config.tokenExpirationTime.toMillis()
        return JWTClaimsSet.Builder()
            .expirationTime(Date(expireTime))
            .notBeforeTime(now)
            .issueTime(now)
            .claim(UID, userContext.uid)
            .claim(USERNAME, userContext.username)
    }

    fun verifyToken(token: String): UserContext {
        val signedJWT = SignedJWT.parse(token)
        val verifier = RSASSAVerifier(config.getRsaPublicKey())
        verifySignature(signedJWT, verifier)
        return getUserContext(signedJWT.jwtClaimsSet)
    }

    private fun getUserContext(jwtClaimsSet: JWTClaimsSet): UserContext {
        val now = Date()
        if (now.after(jwtClaimsSet.expirationTime)) {
            throw ExpiredJwtException()
        }
        return UserContext(
            uid = jwtClaimsSet.getLongClaim(UID) ?: 0,
            username = jwtClaimsSet.getStringClaim(USERNAME) ?: ""
        )
    }

    @Throws(NotVerifiedJwtException::class)
    private fun verifySignature(parse: SignedJWT, verifier: RSASSAVerifier) {
        try {
            if (!parse.verify(verifier)) {
                throw NotVerifiedJwtException()
            }
        } catch (ex: JOSEException) {
            throw NotVerifiedJwtException(ex)
        }
    }
}