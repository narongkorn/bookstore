package com.mycompany.myproject.bookstore.extension

import com.mycompany.myproject.bookstore.client.response.ExternalBookResponse
import com.mycompany.myproject.bookstore.model.response.BookDto
import java.math.BigDecimal

fun ExternalBookResponse.toDto(recommended: Boolean) = BookDto(
    id = id ?: 0L,
    name = bookName.orEmpty(),
    author = authorName.orEmpty(),
    price = (price ?: BigDecimal.ZERO).setScale(2) ,
    isRecommended = recommended
)