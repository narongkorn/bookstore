package com.mycompany.myproject.bookstore.service.impl

import com.mycompany.myproject.bookstore.exception.UserPasswordException
import com.mycompany.myproject.bookstore.security.model.UserContext
import com.mycompany.myproject.bookstore.model.request.LoginRequest
import com.mycompany.myproject.bookstore.model.response.LoginResponse
import com.mycompany.myproject.bookstore.repository.UserRepository
import com.mycompany.myproject.bookstore.security.JwsTokenService
import com.mycompany.myproject.bookstore.service.LoginService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class LoginServiceImpl(private val userRepository: UserRepository, val passwordEncoder: PasswordEncoder, val jwsTokenService: JwsTokenService) : LoginService {

    override fun doLogin(req: LoginRequest): LoginResponse {
        val user = userRepository.findByUsername(req.username)
        if(user != null && passwordEncoder.matches(req.password, user.password)) {
            return LoginResponse(jwsTokenService.createToken(UserContext(uid = user.id!!, username = user.username.orEmpty())))
        }
        throw UserPasswordException()
    }
}