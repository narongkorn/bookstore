package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.model.entity.User
import com.mycompany.myproject.bookstore.model.request.UserCreateRequest
import com.mycompany.myproject.bookstore.model.response.UserDto

interface UserService {
    fun createUser(req: UserCreateRequest)
    fun getUser(userId: Long): UserDto
    fun findUser(userId: Long): User
}