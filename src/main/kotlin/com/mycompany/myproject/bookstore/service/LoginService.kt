package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.model.request.LoginRequest
import com.mycompany.myproject.bookstore.model.response.LoginResponse

interface LoginService {
    fun doLogin(req: LoginRequest): LoginResponse
}