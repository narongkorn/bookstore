package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.model.response.ListBookResponse

interface BookService {
    fun listBook(): ListBookResponse
}