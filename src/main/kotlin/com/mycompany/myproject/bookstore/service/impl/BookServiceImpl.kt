package com.mycompany.myproject.bookstore.service.impl

import com.mycompany.myproject.bookstore.client.ExternalBookClient
import com.mycompany.myproject.bookstore.extension.toDto
import com.mycompany.myproject.bookstore.model.response.BookDto
import com.mycompany.myproject.bookstore.model.response.ListBookResponse
import com.mycompany.myproject.bookstore.service.BookService
import org.springframework.stereotype.Service

@Service
class BookServiceImpl(private val bookClient: ExternalBookClient) : BookService {

    override fun listBook(): ListBookResponse {
        val recommendedBookList = bookClient.listRecommendedBook()
        val anotherBookList = bookClient.listAllBook().filterNot { b ->
            recommendedBookList.any { b.id == it.id }
        }
        val bookList = mutableListOf<BookDto>();
        bookList.addAll(recommendedBookList.map { it.toDto(true) })
        bookList.addAll(anotherBookList.map { it.toDto(false) })
        return ListBookResponse(bookList)
    }
}