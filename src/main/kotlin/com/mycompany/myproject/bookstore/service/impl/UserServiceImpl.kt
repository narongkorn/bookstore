package com.mycompany.myproject.bookstore.service.impl

import com.mycompany.myproject.bookstore.exception.DataNotFoundException
import com.mycompany.myproject.bookstore.exception.UserDuplicationException
import com.mycompany.myproject.bookstore.model.entity.User
import com.mycompany.myproject.bookstore.model.request.UserCreateRequest
import com.mycompany.myproject.bookstore.model.response.UserDto
import com.mycompany.myproject.bookstore.repository.OrderBookRepository
import com.mycompany.myproject.bookstore.repository.UserRepository
import com.mycompany.myproject.bookstore.service.UserService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val orderBookRepository: OrderBookRepository
) : UserService {

    @Transactional
    override fun createUser(req: UserCreateRequest) {
        if (userRepository.findByUsername(req.username) != null) throw UserDuplicationException()
        val fullName = req.username.split(".")
        userRepository.save(
            User(
                username = req.username,
                password = passwordEncoder.encode(req.password),
                name = fullName[0],
                surname = fullName[1],
                dateOfBirth = req.dateOfBirth
            )
        )
    }

    override fun getUser(userId: Long): UserDto {
        val user = findUser(userId)
        val orderBookList = orderBookRepository.findOrderBookByUserId(userId)
        return UserDto(
            name = user.name.orEmpty(),
            surname = user.surname.orEmpty(),
            dateOfBirth = user.dateOfBirth,
            books = orderBookList.map { it.bookId ?: 0L }
        )
    }

    override fun findUser(userId: Long): User {
        return userRepository.findByIdOrNull(userId) ?: throw DataNotFoundException()
    }
}