package com.mycompany.myproject.bookstore.service.impl

import com.mycompany.myproject.bookstore.client.ExternalBookClient
import com.mycompany.myproject.bookstore.exception.DataNotFoundException
import com.mycompany.myproject.bookstore.model.entity.OrderBook
import com.mycompany.myproject.bookstore.model.entity.OrderDetail
import com.mycompany.myproject.bookstore.model.request.OrderCreateRequest
import com.mycompany.myproject.bookstore.model.response.OrderCreateResponse
import com.mycompany.myproject.bookstore.repository.OrderBookRepository
import com.mycompany.myproject.bookstore.repository.OrderDetailRepository
import com.mycompany.myproject.bookstore.service.OrderService
import com.mycompany.myproject.bookstore.service.UserService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.util.Date

@Service
class OrderServiceImpl(
    private val userService: UserService,
    private val orderDetailRepository: OrderDetailRepository,
    private val orderBookRepository: OrderBookRepository,
    private val bookClient: ExternalBookClient
) :
    OrderService {

    @Transactional
    override fun createOrder(req: OrderCreateRequest): OrderCreateResponse {
        val userId = req.userId!!
        val user = userService.findUser(userId)
        val bookList = bookClient.listAllBook()
        var orderDetail = OrderDetail(user = user, createdDate = Date())
        var totalAmount = BigDecimal.ZERO.setScale(2)
        val orderBookList = req.orders.map { bookId ->
            val book = bookList.find { it.id == bookId } ?: throw DataNotFoundException()
            totalAmount = totalAmount.add(book.price)
            OrderBook(order = orderDetail, bookId = book.id)
        }
        orderDetail.orderBookList = orderBookList
        orderDetail.totalAmount = totalAmount
        orderDetail = orderDetailRepository.save(orderDetail)
        return OrderCreateResponse(orderDetail.totalAmount ?: BigDecimal.ZERO)
    }

    @Transactional
    override fun deleteAllOrder(userId: Long) {
        orderBookRepository.deleteOrderBook(userId)
        orderDetailRepository.deleteOrderDetail(userId)
    }
}