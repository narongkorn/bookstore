package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.model.request.OrderCreateRequest
import com.mycompany.myproject.bookstore.model.response.OrderCreateResponse

interface OrderService {
    fun createOrder(req: OrderCreateRequest): OrderCreateResponse
    fun deleteAllOrder(userId: Long)
}