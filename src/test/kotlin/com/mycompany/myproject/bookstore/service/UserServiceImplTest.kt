package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.exception.DataNotFoundException
import com.mycompany.myproject.bookstore.exception.UserDuplicationException
import com.mycompany.myproject.bookstore.model.entity.OrderBook
import com.mycompany.myproject.bookstore.model.entity.User
import com.mycompany.myproject.bookstore.model.request.UserCreateRequest
import com.mycompany.myproject.bookstore.repository.OrderBookRepository
import com.mycompany.myproject.bookstore.repository.UserRepository
import com.mycompany.myproject.bookstore.service.impl.UserServiceImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.Date
import java.util.Optional
import kotlin.test.assertNotNull

@ExtendWith(MockitoExtension::class)
class UserServiceImplTest {

    @InjectMocks
    lateinit var subject: UserServiceImpl

    @Mock
    lateinit var userRepository: UserRepository

    @Mock
    lateinit var passwordEncoder: PasswordEncoder

    @Mock
    lateinit var orderBookRepository: OrderBookRepository

    private val user = User(id = 1, username = "test.test", password = "123456", name = "test", surname = "test")

    @Nested
    inner class CreateOrder {

        private val req = UserCreateRequest(username = "test.test", password = "123456", dateOfBirth = Date())

        @Test
        fun shouldCreateUserSuccess() {
            whenever(userRepository.save(any<User>())).thenReturn(user)

            subject.createUser(req)

            verify(userRepository).findByUsername(any())
            verify(userRepository).save(any<User>())
        }

        @Test
        fun shouldThrowExceptionWhenUserAlreadyRegistered() {
            whenever(userRepository.findByUsername(any())).thenReturn(user)

            assertThrows<UserDuplicationException> {
                subject.createUser(req)
            }

            verify(userRepository).findByUsername(any())
            verify(userRepository, never()).save(any<User>())
        }
    }

    @Nested
    inner class GetUser {

        private val orderBookList = listOf(
            OrderBook(id = 1, bookId = 1), OrderBook(id = 2, bookId = 2)
        )

        @Test
        fun shouldGetUserSuccess() {
            whenever(userRepository.findById(any())).thenReturn(Optional.of(user))
            whenever(orderBookRepository.findOrderBookByUserId(any())).thenReturn(orderBookList)

            val response = subject.getUser(1)

            assertNotNull(response)
            verify(userRepository).findById(any())
            verify(orderBookRepository).findOrderBookByUserId(any())
        }

        @Test
        fun shouldThrowExceptionWhenUserNotFound() {
            whenever(userRepository.findById(any())).thenThrow(DataNotFoundException())

            assertThrows<DataNotFoundException> {
                subject.getUser(1)
            }

            verify(userRepository).findById(any())
            verify(orderBookRepository, never()).findOrderBookByUserId(any())
        }
    }

    @Nested
    inner class FindUser {

        @Test
        fun shouldReturnUserSuccess() {
            whenever(userRepository.findById(any())).thenReturn(Optional.of(user))

            val response = subject.getUser(1)
            assertNotNull(response)

            verify(userRepository).findById(any())
        }

        @Test
        fun shouldThrowExceptionWhenUserNotFound() {
            whenever(userRepository.findById(any())).thenThrow(DataNotFoundException())

            assertThrows<DataNotFoundException> {
                subject.getUser(1)
            }

            verify(userRepository).findById(any())
        }
    }
}