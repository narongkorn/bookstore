package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.client.ExternalBookClient
import com.mycompany.myproject.bookstore.client.response.ExternalBookResponse
import com.mycompany.myproject.bookstore.service.impl.BookServiceImpl
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.math.BigDecimal

@ExtendWith(MockitoExtension::class)
class BookServiceImplTest {

    @InjectMocks
    lateinit var subject: BookServiceImpl

    @Mock
    lateinit var bookClient: ExternalBookClient

    @Nested
    inner class ListBook {

        private val allBookList =
            listOf(ExternalBookResponse(id = 2, bookName = "b", authorName = "b", price = BigDecimal.TEN))
        private val recommendedBookList = listOf(
            ExternalBookResponse(id = 1, bookName = "a", authorName = "a", price = BigDecimal.ONE),
            ExternalBookResponse(id = 2, bookName = "b", authorName = "b", price = BigDecimal.TEN)
        )

        @Test
        fun shouldReturnListBookSuccess() {
            whenever(bookClient.listRecommendedBook()).thenReturn(allBookList)
            whenever(bookClient.listAllBook()).thenReturn(recommendedBookList)

            subject.listBook()

            verify(bookClient).listRecommendedBook()
            verify(bookClient).listAllBook()
        }
    }
}