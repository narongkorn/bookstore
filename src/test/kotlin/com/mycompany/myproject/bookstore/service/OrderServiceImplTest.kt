package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.client.ExternalBookClient
import com.mycompany.myproject.bookstore.client.response.ExternalBookResponse
import com.mycompany.myproject.bookstore.exception.DataNotFoundException
import com.mycompany.myproject.bookstore.model.entity.OrderDetail
import com.mycompany.myproject.bookstore.model.entity.User
import com.mycompany.myproject.bookstore.model.request.OrderCreateRequest
import com.mycompany.myproject.bookstore.repository.OrderBookRepository
import com.mycompany.myproject.bookstore.repository.OrderDetailRepository
import com.mycompany.myproject.bookstore.service.impl.OrderServiceImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.math.BigDecimal
import kotlin.test.assertEquals

@ExtendWith(MockitoExtension::class)
class OrderServiceImplTest {

    @InjectMocks
    lateinit var subject: OrderServiceImpl

    @Mock
    lateinit var userService: UserService

    @Mock
    lateinit var orderDetailRepository: OrderDetailRepository

    @Mock
    lateinit var orderBookRepository: OrderBookRepository

    @Mock
    lateinit var bookClient: ExternalBookClient

    @Nested
    inner class CreateOrder {

        private val req = OrderCreateRequest(orders = listOf(1, 2)).also { it.userId = 1 }
        private val user = User(id = 1, username = "test.test", password = "123456", name = "test", surname = "test")
        private val allBookList =
            listOf(
                ExternalBookResponse(id = 1, bookName = "a", authorName = "a", price = BigDecimal.ONE),
                ExternalBookResponse(id = 2, bookName = "b", authorName = "b", price = BigDecimal.TEN)
            )
        private val orderDetail = OrderDetail(id = 1, totalAmount = BigDecimal.ONE.add(BigDecimal.TEN))
        @Test
        fun shouldCreateOrderSuccess() {
            whenever(userService.findUser(any())).thenReturn(user)
            whenever(bookClient.listAllBook()).thenReturn(allBookList)
            whenever(orderDetailRepository.save(any<OrderDetail>())).thenReturn(orderDetail)

            val response = subject.createOrder(req)

            assertEquals(orderDetail.totalAmount, response.price)
            verify(userService).findUser(any())
            verify(bookClient).listAllBook()
            verify(orderDetailRepository).save(any<OrderDetail>())
        }

        @Test
        fun shouldThrowExceptionWhenDataNotFound() {
            whenever(userService.findUser(any())).thenReturn(user)
            whenever(bookClient.listAllBook()).thenReturn(allBookList)

            assertThrows<DataNotFoundException> {
                subject.createOrder(OrderCreateRequest(orders = listOf(5)).also { it.userId = 1 })
            }

            verify(userService).findUser(any())
            verify(bookClient).listAllBook()
            verify(orderDetailRepository, never()).save(any<OrderDetail>())
        }
    }

    @Nested
    inner class DeleteAllOrder {

        @Test
        fun shouldDeleteSuccess() {
            subject.deleteAllOrder(1)
            verify(orderBookRepository).deleteOrderBook(any())
            verify(orderDetailRepository).deleteOrderDetail(any())

        }
    }
}