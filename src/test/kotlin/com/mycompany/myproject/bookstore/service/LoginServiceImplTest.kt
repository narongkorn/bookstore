package com.mycompany.myproject.bookstore.service

import com.mycompany.myproject.bookstore.exception.UserPasswordException
import com.mycompany.myproject.bookstore.model.entity.User
import com.mycompany.myproject.bookstore.model.request.LoginRequest
import com.mycompany.myproject.bookstore.repository.UserRepository
import com.mycompany.myproject.bookstore.security.JwsTokenService
import com.mycompany.myproject.bookstore.service.impl.LoginServiceImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.crypto.password.PasswordEncoder
import kotlin.test.assertEquals

@ExtendWith(MockitoExtension::class)
class LoginServiceImplTest {

    @InjectMocks
    lateinit var subject: LoginServiceImpl

    @Mock
    lateinit var userRepository: UserRepository

    @Mock
    lateinit var passwordEncoder: PasswordEncoder

    @Mock
    lateinit var jwsTokenService: JwsTokenService

    @Nested
    inner class DoLogin {

        private val req = LoginRequest(username = "test.test", password = "123456")
        private val user = User(id = 1, username = "test.test", password = "123456", name = "test", surname = "test")
        private val token = "ABC"

        @Test
        fun shouldLoginSuccess() {
            whenever(userRepository.findByUsername(any())).thenReturn(user)
            whenever(passwordEncoder.matches(any(), any())).thenReturn(true)
            whenever(jwsTokenService.createToken(any())).thenReturn(token)

            val response = subject.doLogin(req)

            assertEquals(token, response.token)
            verify(userRepository).findByUsername(any())
            verify(passwordEncoder).matches(any(), any())
            verify(jwsTokenService).createToken(any())
        }

        @Test
        fun shouldThrowExceptionWhenUserNotFound() {
            whenever(userRepository.findByUsername(any())).thenReturn(null)

            assertThrows<UserPasswordException> {
                subject.doLogin(req)
            }

            verify(userRepository).findByUsername(any())
            verify(passwordEncoder, never()).matches(any(), any())
            verify(jwsTokenService, never()).createToken(any())
        }

        @Test
        fun shouldThrowExceptionWhenPasswordNotMatch() {
            whenever(userRepository.findByUsername(any())).thenReturn(user)
            whenever(passwordEncoder.matches(any(), any())).thenReturn(false)

            assertThrows<UserPasswordException> {
                subject.doLogin(req)
            }

            verify(userRepository).findByUsername(any())
            verify(passwordEncoder).matches(any(), any())
            verify(jwsTokenService, never()).createToken(any())
        }
    }
}