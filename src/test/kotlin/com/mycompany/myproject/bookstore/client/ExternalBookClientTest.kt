package com.mycompany.myproject.bookstore.client

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@ExtendWith(MockitoExtension::class)
class ExternalBookClientTest(private val bookClient: ExternalBookClient) {

    @Test
    fun listAllBook() {
        val bookList =  bookClient.listAllBook()
        println(bookList.size)
    }

    @Test
    fun listRecommendedBook() {

    }
}