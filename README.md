# Book Store Application #

#### Git lab
link : https://gitlab.com/narongkorn/bookstore.git

##### Run application
`./grawdlew bootRun`

##### Build application
`./grawdlew clean build`

##### Unit test application
`./grawdlew clean test`

##### Swagger UI
link : http://localhost:8080/swagger-ui.html

##### Import postman collection in order to test
link : https://www.getpostman.com/collections/50a18da59d7e6bea34d3

##### Connect database in H2
link : http://localhost:8080/h2-console <br/>
username : sa <br/>
that have no password

##### Curl command
book list : `curl --location --request GET 'http://localhost:8080/books'` <br/>
create user : 
```
curl --location --request POST 'http://localhost:8080/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "den.den",
    "password": "abc",
    "date_of_birth": "20/03/1989"
}'
``` 
login : 
```
curl --location --request POST 'http://localhost:8080/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "den.den",
    "password": "abc"
}'
```
Note : After login successfully then will receive JWT token (JWS token that signed with private key) that has 5 minutes before token expire, this token is used in header for other API

create order : 
```
curl --location --request POST 'http://localhost:8080/users/orders' \
--header 'token: {{TOKEN}}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "orders": [4, 5]
}'
```

get user info : 
```
curl --location --request GET 'http://localhost:8080/users' \
--header 'token: {{TOKEN}}’ 
```

delete order history : 
```
curl --location --request DELETE 'http://localhost:8080/users' \
--header 'token: {{TOKEN}}'
```








